﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldProtection : MonoBehaviour
{
	// if the player touches the shield, 
	// make the protection sprite visible
	// disable collision between player and enemies
	void OnTriggerEnter2D(Collider2D other)
	{
		if ((other.CompareTag("Player")) && (other.gameObject.GetComponent<CharacterController2D>().playerCanMove))
		{
			// do the player collect shield thing
			other.gameObject.GetComponent<CharacterController2D>().TakeShield();

			// destroy the shield
			Destroy(this.gameObject);
		}
	}
}
