﻿using UnityEngine;

public class Key : MonoBehaviour
{
	// if the player touches the key, 
	void OnTriggerEnter2D(Collider2D other)
	{
		if ((other.CompareTag("Player")) && (other.gameObject.GetComponent<CharacterController2D>().playerCanMove))
		{
			// do the player collect key thing
			other.gameObject.GetComponent<CharacterController2D>().TakeKey();

			// destroy the shield
			Destroy(this.gameObject);
		}
	}
}
