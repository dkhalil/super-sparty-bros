﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class FallingPlatform : MonoBehaviour
{
    public float speed; //how fast it shakes
    public float amount; //how much it shakes

    float startTime;
    float delay;
    bool falling = false;
    bool shakeAndFall = false;
    Vector3 spawnLoc;

    // store the layer the player is on (setup in Awake)
    int _platformLayer;


    // Start is called before the first frame update
    void Start()
    {
        startTime = Time.time;
        delay = Random.Range(0f, 2f);
        spawnLoc = transform.position;

        _platformLayer = gameObject.layer;
        Physics2D.IgnoreLayerCollision(_platformLayer, _platformLayer, true);
    }

    // Update is called once per frame
    void Update()
    {
        if (!shakeAndFall || Time.timeScale <= 0) return;

        if (!falling)
        {
            // shaking
            Vector3 v = transform.position;
            v.x += (Mathf.Sin(Time.time * speed) * amount);
            transform.position = v;

            // it's time to fall
            if (Time.time - startTime > delay)
            {
                falling = true;

                gameObject.GetComponent<Rigidbody2D>().isKinematic = false;

                gameObject.transform.DetachChildren();

                StartCoroutine(Respawn());
            }
        }
    }

    IEnumerator Respawn()
    {
        // wait between 1 and 4 seconds before repositioning the falling platform back to its original position
        yield return new WaitForSeconds(Random.Range(1f, 4f));

        // update start time
        startTime = Time.time;

        // reposition
        transform.position = spawnLoc;

        gameObject.GetComponent<Rigidbody2D>().isKinematic = true;

        // start shaking again
        falling = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            // reset start time
            startTime = Time.time;

            shakeAndFall = true;
        }
    }

    private void OnCollisionExit2D(UnityEngine.Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            shakeAndFall = false;
        }
    }
}
