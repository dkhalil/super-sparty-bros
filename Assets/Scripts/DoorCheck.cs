﻿using UnityEngine;

public class DoorCheck : MonoBehaviour
{
	// if the player reaches the door with the key, and the player can move (not dead or victory)
	// then win the level
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Player"))
		{
			CharacterController2D character = other.gameObject.GetComponent<CharacterController2D>();

			if(character.playerCanMove && character.HasKey)
            {
				character.Victory();
			}
		}
	}
}
