﻿using System.Collections;
using UnityEngine;

public class FlyingEye : MonoBehaviour
{
    public Transform[] wayPoints;
	public int damageAmount = 10; // probably deal a lot of damage to kill player immediately
	public AudioClip attackSFX;

	[Range(0f, 10f)]
	public float moveSpeed = 4f;  // enemy move speed when moving

	[Tooltip("How much time in seconds to wait at waypoint")]
	public float waitAtWaypointTime = 1f;   // how long to wait at a waypoint

	[SerializeField]
	int _waypointIndex = 0; // used as index for My_Waypoints

	float _vx = 0f;
	float _moveTime = 0f;

	Rigidbody2D _rigidbody;
	Animator _animator;
	AudioSource _audio;

	// Start is called before the first frame update
	void Start()
    {
		_rigidbody = GetComponent<Rigidbody2D>();
		_animator = GetComponent<Animator>();

		_audio = GetComponent<AudioSource>();
		if (_audio == null)
		{ // if AudioSource is missing
			Debug.LogWarning("AudioSource component missing from this gameobject. Adding one.");
			// let's just add the AudioSource component dynamically
			_audio = gameObject.AddComponent<AudioSource>();
		}
	}

    // Update is called once per frame
    void Update()
    {
		if (Time.time >= _moveTime)
		{
			Flying();
        }
	}

    private void Flying()
    {
		if (wayPoints.Length != 0)
		{
			// make sure the flying eye is facing the waypoint (based on previous movement)
			Flip(_vx);

			// determine distance between waypoint and flying eye
			_vx = wayPoints[_waypointIndex].transform.position.x - transform.position.x;

			// if the flying eye is close enough to waypoint, make its new target the next waypoint
			if (Mathf.Abs(_vx) <= 0.05f)
			{
				// At waypoint so stop moving
				_rigidbody.velocity = new Vector2(0, 0);

				// increment to next index in array
				_waypointIndex++;

				// reset waypoint back to 0 for looping
				if (_waypointIndex >= wayPoints.Length)
				{
					_waypointIndex = 0;
				}

				// setup wait time at current waypoint
				_moveTime = Time.time + waitAtWaypointTime;
			}
			else
			{
				// Set the enemy's velocity to moveSpeed in the x direction.
				_rigidbody.velocity = new Vector2(transform.localScale.x * moveSpeed, _rigidbody.velocity.y);
			}

		}
	}

	// flip the flying eye to face torward the direction he is flying to
	void Flip(float _vx)
	{
		// get the current scale
		Vector3 localScale = transform.localScale;

		if ((_vx > 0f) && (localScale.x < 0f))
			localScale.x *= -1;

		else if ((_vx < 0f) && (localScale.x > 0f))
			localScale.x *= -1;

		// update the scale
		transform.localScale = localScale;
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
		if (collision.CompareTag("Player"))
		{
			_animator.SetTrigger("Attack");

			CharacterController2D player = collision.gameObject.GetComponent<CharacterController2D>();
			Rigidbody2D playerRigidBody = player.GetComponent<Rigidbody2D>();

			if (player.playerCanMove)
			{
				// Make sure the flying eye is facing the player on attack
				Flip(collision.transform.position.x - transform.position.x);

				// attack sound
				PlaySound(attackSFX);

				// stop flying for an instant
				_rigidbody.velocity = new Vector2(0, 0);

				playerRigidBody.isKinematic = true;
				playerRigidBody.velocity = new Vector2(0, 0);

				// apply damage to the player
				StartCoroutine(AttackPlayer(player));

				// stop to enjoy killing the player
				_moveTime = Time.time + 3f;
			}
		}
	}

	IEnumerator AttackPlayer(CharacterController2D player)
    {
		yield return new WaitForSeconds(0.5f);

		player.ApplyDamage(damageAmount);
	}

	void PlaySound(AudioClip clip)
	{
		_audio.PlayOneShot(clip);
	}
}
