﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaKill : MonoBehaviour
{
	public bool destroyNonPlayerObjects = true;

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			// if player then tell the player to do its FallDeath
			other.gameObject.GetComponent<CharacterController2D>().FallDeath();
		}
		else if (destroyNonPlayerObjects)
		{ // not player so just kill object - could be falling enemy for example
			Destroy(other.gameObject);
		}
	}
}
