﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; // include UI namespace so can reference UI elements
using UnityEngine.SceneManagement; // include so we can manipulate SceneManager
using System;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour {

	// static reference to game manager so can be called from other scripts directly (not just through gameobject component)
	public static GameManager gm;

	// levels to move to on victory and lose
	public string levelAfterVictory;
	public string levelAfterGameOver;

	// game performance
	public int score = 0;
	public int highscore = 0;
	public int startLives = 3;
	public int lives = 3;

	// UI elements to control
	public Text UIScore;
	public Text UIHighScore;
	public Text UILevel;
	public GameObject[] UIExtraLives;
	public GameObject UIGamePaused;

	// shield
	public GameObject shieldPrefab;
	// rocket
	public GameObject rocketPrefab;
	// falling platform
	public GameObject fallingPlatformPrefab;

	// private variables
	GameObject _player;
	Vector3 _spawnLocation;
	Scene _scene;

	private int startScore;


	// set things up here
	void Awake () { 
		// setup reference to game manager
		if (gm == null)
			gm = this.GetComponent<GameManager>();

		// setup all the variables, the UI, and provide errors if things not setup properly.
		SetupDefaults();

		StartCoroutine(GenerateShield());

		if (UILevel.text.Equals("Level 3"))
        {
			StartCoroutine(GenerateRocket());
		}

		if(UILevel.text.Equals("Level 4"))
        {
			startScore = score;
            StartCoroutine(CheckVictory());
        }
	}

	// game loop
	void Update() {
		// if ESC pressed then pause the game
		if (Input.GetKeyDown(KeyCode.Escape)) {
			if (Time.timeScale > 0f) {
				UIGamePaused.SetActive(true); // this brings up the pause UI
				Time.timeScale = 0f; // this pauses the game action
				_player.GetComponent<AudioSource>().Pause();
			} else {
				Time.timeScale = 1f; // this unpauses the game action (ie. back to normal)
				UIGamePaused.SetActive(false); // remove the pause UI
				_player.GetComponent<AudioSource>().Play();

			}
		}
	}

	IEnumerator CheckVictory()
    {
		while(true)
        {
			if (score - startScore >= 18)
			{
				_player.GetComponent<CharacterController2D>().Victory();
				break;
			}

			yield return new WaitForEndOfFrame();
		}
    }

	IEnumerator GenerateRocket()
    {
		// as long as the game is running
		while (true)
		{
			while (Time.timeScale < 0f) yield return new WaitForEndOfFrame();

			yield return new WaitForSeconds(Random.Range(2f, 7f));

			Vector3 playerPos = _player.transform.position;
			Vector3 v = new Vector3(Random.Range(playerPos.x - 2, playerPos.x + 2), Random.Range(playerPos.y - 2, playerPos.y + 2), 0);

			GameObject go = Instantiate(rocketPrefab, v, Quaternion.identity);

			// lifetime of rocket is 5 sec
			yield return new WaitForSeconds(5f);

			if (go != null)
				Destroy(go);
		}
	}

	IEnumerator GenerateShield()
    {
		// as long as the game is running
		while(true)
        {
			while (Time.timeScale < 0f) yield return new WaitForEndOfFrame();

			yield return new WaitForSeconds(Random.Range(2f, 7f));
			Vector3 v = new Vector3(0, 0, 0);

			if (UILevel.text.Equals("Level 1"))
				v = new Vector3(Random.Range(-7.0f, 7.0f), 1, 0);

			if (UILevel.text.Equals("Level 2"))
				v = new Vector3(Random.Range(-5.0f, 14f), 2, 0);			
			
			if (UILevel.text.Equals("Level 3"))
            {
				Vector3 playerPos = _player.transform.position;
				v = new Vector3(Random.Range(playerPos.x - 2, playerPos.x + 2), Random.Range(playerPos.y - 2, playerPos.y + 2), 0);
			}

			GameObject go = Instantiate(shieldPrefab, v, Quaternion.identity); 


			// lifetime of shield is 5 sec
			yield return new WaitForSeconds(5f);

			if (go != null)
				Destroy(go);
		}
	}

	// setup all the variables, the UI, and provide errors if things not setup properly.
	void SetupDefaults() {
		// setup reference to player
		if (_player == null)
			_player = GameObject.FindGameObjectWithTag("Player");
		
		if (_player == null)
			Debug.LogError("Player not found in Game Manager");

		// get current scene
		_scene = SceneManager.GetActiveScene();

		// get initial _spawnLocation based on initial position of player
		_spawnLocation = _player.transform.position;

		// if levels not specified, default to current level
		if (levelAfterVictory == "") {
			Debug.LogWarning("levelAfterVictory not specified, defaulted to current level");
			levelAfterVictory = _scene.name;
		}
		
		if (levelAfterGameOver == "") {
			Debug.LogWarning("levelAfterGameOver not specified, defaulted to current level");
			levelAfterGameOver = _scene.name;
		}

		// friendly error messages
		if (UIScore == null)
			Debug.LogError ("Need to set UIScore on Game Manager.");
		
		if (UIHighScore == null)
			Debug.LogError ("Need to set UIHighScore on Game Manager.");
		
		if (UILevel == null)
			Debug.LogError ("Need to set UILevel on Game Manager.");
		
		if (UIGamePaused == null)
			Debug.LogError ("Need to set UIGamePaused on Game Manager.");
		
		// get stored player prefs
		RefreshPlayerState();

		// get the UI ready for the game
		RefreshGUI();
	}

	// get stored Player Prefs if they exist, otherwise go with defaults set on gameObject
	void RefreshPlayerState() {
		lives = PlayerPrefManager.GetLives();

		// special case if lives <= 0 then must be testing in editor, so reset the player prefs
		if (lives <= 0) {
			PlayerPrefManager.ResetPlayerState(startLives, false);
			lives = PlayerPrefManager.GetLives();
		}

		score = PlayerPrefManager.GetScore();
		highscore = PlayerPrefManager.GetHighscore();

		// save that this level has been accessed so the MainMenu can enable it
		PlayerPrefManager.UnlockLevel();
	}

	// refresh all the GUI elements
	void RefreshGUI() {

		// set the text elements of the UI
		UIScore.text = "Score: " + score.ToString();
		UIHighScore.text = "Highscore: " + highscore.ToString();
		UILevel.text = _scene.name;
		
		// turn on the appropriate number of life indicators in the UI based on the number of lives left
		for(int i=0; i < UIExtraLives.Length; i++) {
			if (i < (lives - 1)) { // show one less than the number of lives since you only typically show lives after the current life in UI
				UIExtraLives[i].SetActive(true);
			} else {
				UIExtraLives[i].SetActive(false);
			}
		}
	}

	// public function to add points and update the gui and highscore player prefs accordingly
	public void AddPoints(int amount)
	{
		// increase score
		score += amount;

		// update UI
		UIScore.text = "Score: " + score.ToString();

		// if score > highscore then update the highscore UI too
		if (score > highscore) {
			highscore = score;
			UIHighScore.text = "Highscore: " + score.ToString();
		}
	}

	// public function to remove player life and reset game accordingly
	public void ResetGame() {

		// remove life and update GUI
		lives--;
		RefreshGUI();

		if (lives <= 0) { // no more lives
			// save the current player prefs before going to GameOver
			PlayerPrefManager.SavePlayerState(score, highscore, lives);

			// load the gameOver screen
			SceneManager.LoadScene(levelAfterGameOver);
		} else {
			// tell the player to respawn
			CharacterController2D character = _player.GetComponent<CharacterController2D>();
			character.Respawn(_spawnLocation);
			
			// disable all previously active abilities he had
			GameObject[] abilities = GameObject.FindGameObjectsWithTag("Ability");
            foreach (GameObject go in abilities) go.SetActive(false);
		}
	}

	// public function for level complete
	public void LevelCompete() {
		// save the current player prefs before moving to the next level
		PlayerPrefManager.SavePlayerState(score, highscore, lives);

		// use a coroutine to allow the player to get fanfare before moving to next level
		StartCoroutine(LoadNextLevel()); 
	}
	// load the nextLevel after delay
	IEnumerator LoadNextLevel() {
		yield return new WaitForSeconds(3.5f);
		SceneManager.LoadScene(levelAfterVictory);
	}
}
