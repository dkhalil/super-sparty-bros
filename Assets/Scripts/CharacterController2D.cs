﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement; // include so we can load new scenes

public class CharacterController2D : MonoBehaviour {

	// player controls
	[Range(0.0f, 10.0f)] // create a slider in the editor and set limits on moveSpeed
	public float moveSpeed = 3f;

	public float jumpForce = 600f;

	// player health
	public int playerHealth = 1;

	// LayerMask to determine what is considered ground for the player
	public LayerMask whatIsGround;

	// Transform just below feet for checking if player is grounded
	public Transform groundCheck;

	// the protection GO 
	public GameObject protectionGO;
	// the rocket power up GO
	public GameObject powerUpGo;

	// player can move?
	// we want this public so other scripts can access it but we don't want to show in editor as it might confuse designer
	[HideInInspector]
	public bool playerCanMove = true;

	// SFXs
	public AudioClip coinSFX;
	public AudioClip deathSFX;
	public AudioClip fallSFX;
	public AudioClip jumpSFX;
	public AudioClip victorySFX;
	public AudioClip shieldSFX;
	public AudioClip keySFX;
	public AudioClip rocketSFX;

	public bool HasKey { get; set; }

    // private variables below

    // store references to components on the gameObject
    Transform _transform;
	Rigidbody2D _rigidbody;
	Animator _animator;
	AudioSource _audio;

	// hold player motion in this timestep
	float _vx;
	float _vy;

	// player tracking
	bool facingRight = true;
	bool isGrounded = false;
	bool isRunning = false;
	bool canDoubleJump = false;

	// store the layer the player is on (setup in Awake)
	int _playerLayer;

	// number of layer that Platforms are on (setup in Awake)
	int _platformLayer;

	// number of layer that Enemies are on
	int _enemyLayer;
	
	void Awake () {
		// get a reference to the components we are going to be changing and store a reference for efficiency purposes
		_transform = GetComponent<Transform> ();
		
		_rigidbody = GetComponent<Rigidbody2D> ();
		if (_rigidbody==null) // if Rigidbody is missing
			Debug.LogError("Rigidbody2D component missing from this gameobject");
		
		_animator = GetComponent<Animator>();
		if (_animator==null) // if Animator is missing
			Debug.LogError("Animator component missing from this gameobject");
		
		_audio = GetComponent<AudioSource> ();
		if (_audio==null) { // if AudioSource is missing
			Debug.LogWarning("AudioSource component missing from this gameobject. Adding one.");
			// let's just add the AudioSource component dynamically
			_audio = gameObject.AddComponent<AudioSource>();
		}

		// determine the player's specified layer
		_playerLayer = this.gameObject.layer;

		// determine the platform's specified layer
		_platformLayer = LayerMask.NameToLayer("Platform");

		_enemyLayer = LayerMask.NameToLayer("Enemy");
	}

	// this is where most of the player controller magic happens each game event loop
	void Update()
	{
		// exit update if player cannot move or game is paused
		if (!playerCanMove || (Time.timeScale == 0f))
			return;

		// determine horizontal velocity change based on the horizontal input
		_vx = Input.GetAxisRaw ("Horizontal");

		// Determine if running based on the horizontal movement
		if (_vx != 0) 
		{
			isRunning = true;
		} else {
			isRunning = false;
		}

		// set the running animation state
		_animator.SetBool("Running", isRunning);

		// get the current vertical velocity from the rigidbody component
		_vy = _rigidbody.velocity.y;

		// Check to see if character is grounded by raycasting from the middle of the player
		// down to the groundCheck position and see if collected with gameobjects on the
		// whatIsGround layer
		isGrounded = Physics2D.Linecast(_transform.position, groundCheck.position, whatIsGround);

        // allow double jump after grounded
        if (isGrounded)
        {
			canDoubleJump = true;
        }

		// Set the grounded animation states
		_animator.SetBool("Grounded", isGrounded);

		if(isGrounded && Input.GetButtonDown("Jump")) // If grounded AND jump button pressed, then allow the player to jump
		{
			DoJump();
		} 
		// next frame
		else if(canDoubleJump && Input.GetButtonDown("Jump")) // if can double jump AND jump button pressed, then allow the player to double jump 
        {
			DoJump();
			// disable double jump after double jumping since we can do it only once
			canDoubleJump = false;
        }
	
		// If the player stops jumping mid jump and player is not yet falling
		// then set the vertical velocity to 0 (he will start to fall from gravity)
		if(Input.GetButtonUp("Jump") && _vy > 0f)
		{
			_vy = 0f;
		}

		// Change the actual velocity on the rigidbody
		_rigidbody.velocity = new Vector2(_vx * moveSpeed, _vy);

		// if moving up then don't collide with platform layer
		// this allows the player to jump up through things on the platform layer
		// NOTE: requires the platforms to be on a layer named "Platform"
		Physics2D.IgnoreLayerCollision(_playerLayer, _platformLayer, (_vy > 0.0f)); 
		
	}

	// Checking to see if the sprite should be flipped
	// this is done in LateUpdate since the Animator may override the localScale
	// this code will flip the player even if the animator is controlling scale
	void LateUpdate()
	{
		// get the current scale
		Vector3 localScale = _transform.localScale;

		if (_vx > 0) // moving right so face right
		{
			facingRight = true;
		} else if (_vx < 0) { // moving left so face left
			facingRight = false;
		}

		// check to see if scale x is right for the player
		// if not, multiple by -1 which is an easy way to flip a sprite
		if (((facingRight) && (localScale.x<0)) || ((!facingRight) && (localScale.x>0))) {
			localScale.x *= -1;
		}

		// update the scale
		_transform.localScale = localScale;
	}

	// if the player collides with a MovingPlatform, then make it a child of that platform
	// so it will go for a ride on the MovingPlatform
	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag("MovingPlatform") || other.gameObject.CompareTag("FallingPlatform")) 
		{
			this.transform.parent = other.transform;
		}
	}

	// if the player exits a collision with a moving platform, then unchild it
	void OnCollisionExit2D(Collision2D other)
	{
		if (other.gameObject.CompareTag("MovingPlatform"))
		{
			this.transform.parent = null;
		}
	}

	// make the player jump
	private void DoJump()
    {
		// reset current vertical motion to 0 prior to jump
		_vy = 0f;
		// add a force in the up direction
		_rigidbody.AddForce(new Vector2(0, jumpForce));
		// play the jump sound
		PlaySound(jumpSFX);
	}

	// do what needs to be done to freeze the player
 	void FreezeMotion() {
		playerCanMove = false;
        _rigidbody.velocity = new Vector2(0,0);
		_rigidbody.isKinematic = true;
	}

	// do what needs to be done to unfreeze the player
	void UnFreezeMotion() {
		playerCanMove = true;
		_rigidbody.isKinematic = false;
	}

	// play sound through the audiosource on the gameobject
	void PlaySound(AudioClip clip)
	{
		_audio.PlayOneShot(clip);
	}

	// public function to apply damage to the player
	public void ApplyDamage(int damage) {
		if (playerCanMove) {
			playerHealth -= damage;

			if (playerHealth <= 0) { // player is now dead, so start dying
				PlaySound(deathSFX);
				StartCoroutine (KillPlayer());
			}
		}
	}

	// public function to kill the player when they have a fall death
	public void FallDeath() {
		if (playerCanMove) {
			playerHealth = 0;
			PlaySound(fallSFX);
			StartCoroutine (KillPlayer());
		}
	}

	// coroutine to kill the player
	IEnumerator KillPlayer()
	{
		if (playerCanMove)
		{
			// freeze the player
			FreezeMotion();

			// play the death animation
			_animator.SetTrigger("Death");
			
			// After waiting tell the GameManager to reset the game
			yield return new WaitForSeconds(2.0f);

			if (GameManager.gm) // if the gameManager is available, tell it to reset the game
				GameManager.gm.ResetGame();
			else // otherwise, just reload the current level
				SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		}
	}

	public void CollectCoin(int amount) {
		PlaySound(coinSFX);

		if (GameManager.gm) // add the points through the game manager, if it is available
			GameManager.gm.AddPoints(amount);
	}

	public void TakeShield()
	{
		if(shieldSFX != null)
			PlaySound(shieldSFX);

		StartCoroutine(EnableProtection());

	}

	public void TakeKey()
	{
		if (keySFX != null)
			PlaySound(keySFX);

		HasKey = true;

	}
	public void TakeRocket()
	{
		if (rocketSFX != null)
			PlaySound(rocketSFX);

		float speed = 0f; 
		float maxSpeed = 10f; //This is the maximum speed that the object will achieve
		float acceleration = 2f; //How fast will object reach a maximum speed
		float deceleration = 2f; //How fast will object reach a speed of 0

		powerUpGo.SetActive(true);

        StartCoroutine(FlyInTheAir(speed, maxSpeed, acceleration, deceleration));
	}

	IEnumerator FlyInTheAir(float speed, float maxSpeed, float accelaration, float deceleration)
    {
		float currentTime = Time.time;
		bool accelerating = true;

		while(Time.time - currentTime < 3f)
        {
			while (Time.timeScale <= 0) yield return new WaitForEndOfFrame();

			if (speed == maxSpeed) accelerating = false;

			else
			{
				if (speed < maxSpeed && accelerating)
				{
					speed += accelaration * (Time.time - currentTime)/* * Time.fixedDeltaTime*/;
				}
                else
                {
                    speed -= deceleration * (Time.time - currentTime)/* * Time.fixedDeltaTime*/;
                }
            }
			//transform.position += new Vector3(0, speed / 10, 0);

			_rigidbody.AddForce(new Vector2(0, speed * 5));

            yield return new WaitForEndOfFrame();
        }

		powerUpGo.SetActive(false);

		_audio.Stop();
	}

	IEnumerator EnableProtection()
    {
		protectionGO.SetActive(true); // activate for certain amount of time
		Physics2D.IgnoreLayerCollision(_playerLayer, _enemyLayer, true);

		// disable collision after 5 sec
		yield return new WaitForSeconds(5f);

		protectionGO.SetActive(false);
		Physics2D.IgnoreLayerCollision(_playerLayer, _enemyLayer, false);

	}

	// public function on victory over the level
	public void Victory() {
		PlaySound(victorySFX);
		FreezeMotion();
		_animator.SetTrigger("Victory");

		if (GameManager.gm) // do the game manager level compete stuff, if it is available
			GameManager.gm.LevelCompete();
	}

	// public function to respawn the player at the appropriate location
	public void Respawn(Vector3 spawnloc) {
		_audio.Stop();
		StopAllCoroutines();
		UnFreezeMotion();
		playerHealth = 1;
		_transform.parent = null;
		_transform.position = spawnloc;
		_animator.SetTrigger("Respawn");
	}

	public void EnemyBounce()
    {
		DoJump();
    }
}
