﻿using UnityEngine;

public class RocketPowerUp : MonoBehaviour
{
	// if the player touches the rocket and the player can move (not dead or victory)
	// then take the rocket
	void OnTriggerEnter2D(Collider2D other)
	{
		if ((other.CompareTag("Player")) && (other.gameObject.GetComponent<CharacterController2D>().playerCanMove))
		{
			// collect rocket (velocity up)
			other.gameObject.GetComponent<CharacterController2D>().TakeRocket();

			// destroy the rocket
			Destroy(this.gameObject);
		}
	}
}
