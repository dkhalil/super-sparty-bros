﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goblin : MonoBehaviour
{
	public int damageAmount = 10; // probably deal a lot of damage to kill player immediately
	public AudioClip attackSFX;
	
	Animator _animator;
	AudioSource _audio;

	// Start is called before the first frame update
	void Start()
    {
		_animator = GetComponent<Animator>();

		_audio = GetComponent<AudioSource>();
		if (_audio == null)
		{ // if AudioSource is missing
			Debug.LogWarning("AudioSource component missing from this gameobject. Adding one.");
			// let's just add the AudioSource component dynamically
			_audio = gameObject.AddComponent<AudioSource>();
		}
	}

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.CompareTag("Player"))
		{
			_animator.SetTrigger("Attack");

			CharacterController2D player = collision.gameObject.GetComponent<CharacterController2D>();
			Rigidbody2D playerRigidBody = player.GetComponent<Rigidbody2D>();

			if (player.playerCanMove)
			{
				// Make sure the flying eye is facing the player on attack
				Flip(collision.transform.position.x - transform.position.x);

				playerRigidBody.isKinematic = true;
				playerRigidBody.velocity = new Vector2(0, 0);

				// apply damage to the player
				StartCoroutine(AttackPlayer(player));
			}
		}
	}

	IEnumerator AttackPlayer(CharacterController2D player)
	{
		yield return new WaitForSeconds(.5f);

		// attack sound
		PlaySound(attackSFX);

		player.ApplyDamage(damageAmount);
	}

	void Flip(float _vx)
	{
		// get the current scale
		Vector3 localScale = transform.localScale;

		if ((_vx > 0f) && (localScale.x < 0f))
			localScale.x *= -1;

		else if ((_vx < 0f) && (localScale.x > 0f))
			localScale.x *= -1;

		// update the scale
		transform.localScale = localScale;
	}

	void PlaySound(AudioClip clip)
	{
		_audio.PlayOneShot(clip);
	}
}
